package persistant;

public class Task1 {
    static int showandsumOdd(int startNumber, int endNumber) {
        int oddSum = 0;
        System.out.println("The Odd Numbers are:");
        for (int i = startNumber; i <= endNumber; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                oddSum += i;
            }
        }
        System.out.println();
        System.out.println("The total of Odd numbers is : " + oddSum);
        return oddSum;
    }
    static int showandsumEven(int startNumber, int endNumber) {
        int evenSum = 0;
        System.out.println("The Even Numbers are:");
        for (int i = endNumber; i >= startNumber; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                evenSum += i;
            }
        }
        System.out.println();
        System.out.println("The total of Even numbers is : " + evenSum);
        return evenSum;
    }
}
